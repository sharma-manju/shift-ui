﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetriCam;
using MetriCam.Utilities;
using System.Runtime.InteropServices;
using System.Collections;



namespace Shift
{

   
    public partial class Shift : Form
    {

        Timer t = new Timer();

        int WIDTH = 300, HEIGHT = 300, HAND = 250;

        int u;  //in degree
        int cx, cy;     //center of the circle
        int x, y;       //HAND coordinate

        int tx, ty, lim = 20;
        Bitmap bmp;
        Pen PEN;
        Graphics g;

        public Shift()
        {

            InitializeComponent();
        }
        WebCam oWebCam = new WebCam();
        private object a;

        private void Shift_Load(object sender, EventArgs e)
        {
            //create Bitmap
            bmp = new Bitmap(WIDTH + 1, HEIGHT + 1);

            //background color
            this.BackColor = Color.White;

            //center
            cx = WIDTH / 2;
            cy = HEIGHT / 2;



            //initial degree of HAND
            u = 0;

            //timer
            t.Interval = 30; //in millisecond
            t.Tick += new EventHandler(this.t_Tick);
            t.Start();
        }
            private void t_Tick(object sender, EventArgs e)
            {
                //pen
                PEN = new Pen(Color.Green, 1f);

                //graphics
                g = Graphics.FromImage(bmp);

                //calculate x, y coordinate of HAND
                int tu = (u - lim) % 360;

                if (u >= 0 && u <= 180)
                {
                    //right half
                    //u in degree is converted into radian.

                    x = cx + (int)(HAND * Math.Sin(Math.PI * u / 180));
                    y = cy - (int)(HAND * Math.Cos(Math.PI * u / 180));
                }
                else
                {
                    x = cx - (int)(HAND * -Math.Sin(Math.PI * u / 180));
                    y = cy - (int)(HAND * Math.Cos(Math.PI * u / 180));
                }

                if (tu >= 0 && tu <= 180)
                {
                    //right half
                    //tu in degree is converted into radian.

                    tx = cx + (int)(HAND * Math.Sin(Math.PI * tu / 180));
                    ty = cy - (int)(HAND * Math.Cos(Math.PI * tu / 180));
                }
                else
                {
                    tx = cx - (int)(HAND * -Math.Sin(Math.PI * tu / 180));
                    ty = cy - (int)(HAND * Math.Cos(Math.PI * tu / 180));
                }

                //draw circle
                g.DrawEllipse(PEN, 0, 0, WIDTH, HEIGHT);  //bigger circle
                g.DrawEllipse(PEN, 80, 80, WIDTH - 160, HEIGHT - 160);    //smaller circle

                //draw perpendicular line
                g.DrawLine(PEN, new Point(cx, 0), new Point(cx, HEIGHT)); // UP-DOWN
                g.DrawLine(PEN, new Point(0, cy), new Point(WIDTH, cy)); //LEFT-RIGHT

                //draw HAND
                g.DrawLine(new Pen(Color.Black, 1f), new Point(cx, cy), new Point(tx, ty));
                g.DrawLine(PEN, new Point(cx, cy), new Point(x, y));

                //load bitmap in picturebox2
                pictureBox2.Image = bmp;

                //dispose
                PEN.Dispose();
                g.Dispose();

                //update
                u++;
                if (u == 360)
                {
                    u = 0;
                }


                this.label1.Text =
    this.monthCalendar1.SelectionRange.Start.ToShortDateString();
                timer1.Start();
                label2.Text = DateTime.Now.ToLongTimeString();
                oWebCam.Container = pictureBox1;

            }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            



        }



        private void btnStart_Click(object sender, EventArgs e)
        {
            
            oWebCam.OpenConnection();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            oWebCam.SaveImage();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            oWebCam.Dispose();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("strength to stand/less fluctuation < stability");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("maximum distance");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Compare frequency difference b/w left and right foot");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("speed");
        }

        
        private void pictureBox2_Click_1(object sender, EventArgs e)
        {

            MessageBox.Show("heatmap/colorcount as per the rows and columns");

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            this.label1.Text =
    this.monthCalendar1.SelectionRange.Start.ToLongDateString();

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label2.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();

        }

        private void label2_Click(object sender, EventArgs e)
        {
            label2.Text = DateTime.Now.ToLongTimeString();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Frequency Graph to show peak value");
        }

        //private void Shift_Load(object sender, EventArgs e)
        //{
        //    oWebCam.Container = pictureBox1;
        //}
    }
}
